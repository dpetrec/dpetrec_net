﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrec_i3.Data
{
    public class RadnikDb : DbContext
    {
        public RadnikDb(DbContextOptions<RadnikDb> options)
           : base(options)
        {

        }

        public DbSet<Radnik> Radnik { get; set; }
    }
}
