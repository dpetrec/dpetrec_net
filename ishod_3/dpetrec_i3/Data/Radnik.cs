﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrec_i3.Data
{
    public class Radnik
    {
        [Key]
        [Required]
        public int ID { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Mjesto { get; set; }
        public int Placa { get; set; }
    }
}
