﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrec_i3.Data
{
    public class RadnikRadnje
    {
        private readonly RadnikDb _dbrad;

        public RadnikRadnje(RadnikDb db)
        {
            _dbrad = db;

        }

        //Get 

        public List<Radnik> DohvatiRadnike()
        {
            var list = _dbrad.Radnik.ToList();
            return list;
        }

        //Insert

        public string Kreiraj(Radnik radnik)
        {
            _dbrad.Radnik.Add(radnik);
            _dbrad.SaveChanges();
            return "Kreiran";
        }

        //GetByID
        public Radnik DohvatiRadnikaID(int id)
        {
            Radnik radnik = _dbrad.Radnik.FirstOrDefault(x => x.ID== id);
            return radnik;
        }

        //Update
        public string UpdateRadnika(Radnik radnik)
        {
            _dbrad.Radnik.Update(radnik);
            _dbrad.SaveChanges();
            return "Update odraden";

        }

        //Delete
        public string BrisanjeRadnika(Radnik radnik)
        {
            _dbrad.Radnik.Remove(radnik);
            _dbrad.SaveChanges();
            return "Uspjesno obrisan";
        }

    }
}
