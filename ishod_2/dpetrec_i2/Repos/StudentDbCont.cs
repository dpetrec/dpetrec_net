﻿using dpetrec_i2.Repos.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrec_i2.Repos
{
    public class StudentDbCont : DbContext
    {
        public StudentDbCont()
        {
        }

        public StudentDbCont(DbContextOptions<StudentDbCont> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=sql.vub.zone,14330;Initial Catalog=dpetrec_i2;User ID=sa;Password=VUBserver2020");
            }
        }

        public DbSet<Student> Student { get; set; }
        public DbSet<Predmet> Predmet { get; set; }
        public DbSet<PohadanjePredmeta> PohadanjePredmeta { get; set; }
    }
}
