﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrec_i2.Repos.Models
{
    public class PohadanjePredmeta
    {
        public int ID { get; set; }
        public string NazivPredmeta { get; set; }
        public int JMBAG { get; set; }

    }
}
