﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrec_i2.Repos.Models
{
    public class Predmet
    {
        public int ID { get; set; }
        public string NazivPredmeta { get; set; }
        public string OpisPredmeta { get; set; }
        public int ECTS { get; set; }

    }
}
