﻿using dpetrec_i2.Repos;
using dpetrec_i2.Repos.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrec_i2.Controllers
{
    public class PohadanjePredmetaController : Controller
    {
        StudentDbCont db = new StudentDbCont();
        // GET: PohadanjePredmetaController
        public ActionResult Index()
        {
            return View(db.PohadanjePredmeta.ToList());
        }

        // GET: PohadanjePredmetaController/Details/5
        public ActionResult Details(int id)
        {
            PohadanjePredmeta predmetPoh = db.PohadanjePredmeta.Find(id);
            if (predmetPoh == null)
            {
                return RedirectToAction("Home");
            }
            return View(predmetPoh);
        }

        // GET: PohadanjePredmetaController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PohadanjePredmetaController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PohadanjePredmeta predmetPoh)
        {
            if (ModelState.IsValid)
            {
                db.PohadanjePredmeta.Add(predmetPoh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(predmetPoh);
        }

        // GET: PohadanjePredmetaController/Edit/5
        public ActionResult Edit(int id)
        {
            PohadanjePredmeta predmetPoh = db.PohadanjePredmeta.Find(id);
            if (predmetPoh == null)
            {
                RedirectToAction("Index");
            }
            return View(predmetPoh);
        }

        // POST: PohadanjePredmetaController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PohadanjePredmeta predmetPoh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(predmetPoh).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            return View(predmetPoh);
        }

        // GET: PohadanjePredmetaController/Delete/5
        public ActionResult Delete(int id)
        {
            var predmetPoh = db.PohadanjePredmeta.Where(x => x.ID == id).FirstOrDefault();
            if (predmetPoh == null)
            {
                return RedirectToAction("Index");
            }
            return View(predmetPoh);
        }

        // POST: PohadanjePredmetaController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePohPred(int id)
        {
            var predmetPoh = db.PohadanjePredmeta.Find(id);
            db.PohadanjePredmeta.Remove(predmetPoh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
