﻿using dpetrec_i2.Repos;
using dpetrec_i2.Repos.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrec_i2.Controllers
{
    public class PredmetController : Controller
    {
        StudentDbCont db = new StudentDbCont();
        // GET: PredmetController
        public ActionResult Index()
        {
            return View(db.Predmet.ToList());
        }

        // GET: PredmetController/Details/5
        public ActionResult Details(int id)
        {
            Predmet predmet = db.Predmet.Find(id);
            if (predmet == null)
            {
                return RedirectToAction("Home");
            }
            return View(predmet);
        }

        // GET: PredmetController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PredmetController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Predmet predmet)
        {
            if (ModelState.IsValid)
            {
                db.Predmet.Add(predmet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(predmet);
        }

        // GET: PredmetController/Edit/5
        public ActionResult Edit(int id)
        {
            Predmet predmet = db.Predmet.Find(id);
            if (predmet == null)
            {
                RedirectToAction("Index");
            }
            return View(predmet);
        }

        // POST: PredmetController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Predmet predmet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(predmet).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            return View(predmet);
        }

        // GET: PredmetController/Delete/5
        public ActionResult Delete(int id)
        {
            var predmet = db.Predmet.Where(x => x.ID == id).FirstOrDefault();
            
            if (predmet == null)
            {
                return RedirectToAction("Index");
            }
            return View(predmet);
        }

        // POST: PredmetController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePredmet(int id)
        {
            var predmet = db.Predmet.Find(id);
            
            db.Predmet.Remove(predmet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
