﻿using dpetrec_i2.Repos;
using dpetrec_i2.Repos.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrec_i2.Controllers
{
    public class StudentController : Controller
    {
        StudentDbCont db = new StudentDbCont();
        // GET: StudentController
        public ActionResult Index()
        {
            return View(db.Student.ToList());

        }

        // GET: StudentController/Details/5
        public ActionResult Details(int id)
        {
            Student student = db.Student.Find(id);
            if (student == null)
            {
                return RedirectToAction("Home");
            }
            return View(student);
        }

        // GET: StudentController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StudentController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Student student)
        {
            if (ModelState.IsValid)
            {
                db.Student.Add(student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(student);
        }

        // GET: StudentController/Edit/5
        public ActionResult Edit(int id)
        {
            Student student = db.Student.Find(id);
            if (student == null)
            {
                RedirectToAction("Index");
            }
            return View(student);
        }

        // POST: StudentController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Student student)
        {
            if (ModelState.IsValid)
            {
                db.Entry(student).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            return View(student);
        }

        // GET: StudentController/Delete/5
        public ActionResult Delete(int id)
        {
            var student = db.Student.Where(x => x.ID == id).FirstOrDefault();
            if (student == null)
            {
                return RedirectToAction("Index");
            }
            return View(student);
        }

        // POST: StudentController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteStudent(int id)
        {
            var student = db.Student.Find(id);
            db.Student.Remove(student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
