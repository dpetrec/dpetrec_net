using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dpetrec_i1.Repos;
using dpetrec_i1.Repos.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace dpetrec_i1.Pages.Sections
{
    public class DetailsModel : PageModel
    {
        private readonly HardGymDbContext _context;

        public DetailsModel(HardGymDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public GymMember member { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            member = await _context.HardGym.FirstOrDefaultAsync(m => m.ID == id);

            if (member == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
