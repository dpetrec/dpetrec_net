using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dpetrec_i1.Repos;
using dpetrec_i1.Repos.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace dpetrec_i1.Pages.Sections
{
    public class EditModel : PageModel
    {
        private readonly HardGymDbContext _context;

        public EditModel(HardGymDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public GymMember member { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            member = await _context.HardGym.FirstOrDefaultAsync(m => m.ID == id);

            if (member == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(member).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostojiClan(member.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool PostojiClan(int? id)
        {
            return _context.HardGym.Any(e => e.ID == id);
        }
    }
}
