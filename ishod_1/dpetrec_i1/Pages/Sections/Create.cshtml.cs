using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dpetrec_i1.Repos;
using dpetrec_i1.Repos.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace dpetrec_i1.Pages.Sections
{
    public class CreateModel : PageModel
    {
        private readonly HardGymDbContext dbcon;

        public CreateModel(HardGymDbContext context)
        {
            dbcon = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public GymMember member { get; set; }
     
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
        
            dbcon.HardGym.Add(member);
            await dbcon.SaveChangesAsync();
            return RedirectToPage("./Index");
        }
    }
}
