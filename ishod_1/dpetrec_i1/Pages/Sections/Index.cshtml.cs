using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dpetrec_i1.Repos;
using dpetrec_i1.Repos.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace dpetrec_i1.Pages.Sections
{
    public class IndexModel : PageModel
    {
        private readonly HardGymDbContext _context;

        public IndexModel(HardGymDbContext context)
        {
            _context = context;
        }

        public IList<GymMember> member { get; set; }

        public async Task OnGetAsync()
        {
            member = await _context.HardGym.ToListAsync();
        }
    }
}
