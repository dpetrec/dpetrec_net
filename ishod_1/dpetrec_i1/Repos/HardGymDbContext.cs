﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dpetrec_i1.Repos.Models;

namespace dpetrec_i1.Repos
{
    public partial class HardGymDbContext : DbContext
    {
        public HardGymDbContext(DbContextOptions<HardGymDbContext> options)
           : base(options)
        {

        }

        public DbSet<GymMember> HardGym { get; set; }
    }
}
