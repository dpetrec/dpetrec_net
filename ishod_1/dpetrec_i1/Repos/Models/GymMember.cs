﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrec_i1.Repos.Models
{
    [Table("HardGym")]
    public partial class GymMember
    {
        [Key]
        [Column("ID")]
        public int? ID { get; set; }     
        public string NazivClana { get; set; }      
        public string Podruznica { get; set; }          
        public string Gymbag { get; set; }     
        public int iznosClanarine { get; set; }



    }
}
