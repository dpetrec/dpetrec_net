﻿using dpetrecI4.Models;
using dpetrecI4.Repos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrecI4.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : Controller
    {
        private readonly IStudentRepos _studentRepos;

        public StudentController(IStudentRepos studentRepos)
        {
            _studentRepos = studentRepos;
        }

        [HttpGet]
        public async Task<IEnumerable<Student>> GetStudents()
        {
            return await _studentRepos.Get();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Student>> GetStudents(int id)
        {
            return await _studentRepos.Get(id);
        }

        [HttpPost]
        public async Task<ActionResult<Student>> PostStudents([FromBody] Student student)
        {
            var studentNew = await _studentRepos.Create(student);
            return CreatedAtAction(nameof(GetStudents), new { id = studentNew.ID }, student);

        }

        [HttpPut]
        public async Task<ActionResult> PutStudents(int id, [FromBody] Student students)
        {
            if (id != students.ID)
            {
                return BadRequest();
            }

            await _studentRepos.Update(students);

            return NoContent();
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            var StudDelete = await _studentRepos.Get(id);
            if (StudDelete == null)
                return NotFound();

            await _studentRepos.Delete(StudDelete.ID);
            return NoContent();
        }
    }
}
