﻿using dpetrecI4.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dpetrecI4.Repos
{
    public class StudentRepos : IStudentRepos
    {
        private readonly StudentDb db;

        public StudentRepos(StudentDb context)
        {
            db = context;
        }
        public async Task<Student> Create(Student student)
        {
            db.Student.Add(student);
            await db.SaveChangesAsync();

            return student;
        }

        public async Task Delete(int id)
        {
            var studDelete = await db.Student.FindAsync(id);
            db.Student.Remove(studDelete);
            await db.SaveChangesAsync();
        }

        public async Task<IEnumerable<Student>> Get()
        {
            return await db.Student.ToListAsync();

        }

        public async Task<Student> Get(int id)
        {
            return await db.Student.FindAsync(id);

        }

        public async Task Update(Student student)
        {
            db.Entry(student).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }
    }
}
